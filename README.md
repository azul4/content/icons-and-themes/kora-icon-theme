# kora-icon-theme

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

SVG icon theme suitable for every desktop environment (dark and light versions, HiDPI support)

https://github.com/bikass/kora

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/icons-and-themes/kora-icon-theme.git
```
